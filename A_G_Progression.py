"""
This is a program created to evaluate the sequence of an arithmetic or geometric from (n,0) and to find the sum
of that sequence. Program created by Apparate Code

The function of this program is as follows:

Given the arithmetic/geometric sequence (a,b,c,d,e,f,g.....)

1. Find the sum of the sequence
2. Find the rest of the sequence from (0,n)
"""

print("What the type of sequence is considered?")

def choice_selection():
        sequence_type = input("Press a if arithmetic and g if geometric: ")
        
        if str(sequence_type) == "a":
                
                #Brief explanation of the progression
                print("Arithmetic equation is as follows: F = a + (n-1)b")
                print("Where F is the sequence to n-1, a is the sequence n=0 and b is the difference between two consecutive sequence")
                arithmetic_selection()
                
        elif str(sequence_type) == "g":
                
                #Brief explanation of the progression
                print("Geometric equation is as follows: F = (a)(r^(n-1)")
                print("Where F is the sequence to n, a is the sequence n=1 and b is the ratio between two consecutive sequence")
                geometric_selection()
                
        else:
                choice_selection()

#If arithmetic progression is the choice
def arithmetic_selection():

        #Empty dictionary to hold the progression
        arithmetic_list = []
        arithmetic_a_value = input("What is the value of a? ")
        arithmetic_b_value = input("What is the value of b? ")
        arithmetic_n_value = input("What is the maximum value of n? ")
        arithmetic_a_value = int(arithmetic_a_value)
        arithmetic_b_value = int(arithmetic_b_value)
        arithmetic_n_value = int(arithmetic_n_value) + 1
        arithmetic_counter = 1 #This is so that the progression starts from 0 as seen from the equation
        
        while arithmetic_counter < arithmetic_n_value:
                sequence_value = arithmetic_a_value + arithmetic_b_value*(arithmetic_counter - 1) # <<This is the reason for arithmetic_counter = 1
                arithmetic_counter = arithmetic_counter + 1
                arithmetic_list.append(sequence_value)
                
        print(arithmetic_list)
        choice_selection()

#If geometric progression is the choice
def geometric_selection():

        #Empty dictionary to hold the progression
        geometric_list = []
        geometric_a_value = input("What is the value of a? ")
        geometric_r_value = input("What is the value of r? ")
        geometric_n_value = input("What is the value of n? ")
        geometric_a_value = int(geometric_a_value)
        geometric_r_value = int(geometric_r_value)
        geometric_n_value = int(geometric_n_value) + 1
        geometric_counter = 1 #This is so that the progression starts from 0 as seen from the equation
        
        while geometric_counter < geometric_n_value:
                sequence_value = geometric_a_value * (geometric_r_value ** (geometric_counter - 1)) #<< This is the reason for geometric_counter = 1
                geometric_counter = geometric_counter + 1
                geometric_list.append(sequence_value)
                
        print(geometric_list)
        choice_selection()

choice_selection()

"""
Issues:
1. Present sequence in ascending order of n - DONE
2. Calculate geometric sequence - DONE
3. Provide any 2 consecutive sequence and which n the number is, compute the whole sequence
4. Documentation - DONE
5. Explanation of the progression repeats every time --> use counter to prevent this

Ideas:
a. Create software to determine the quadratic equation.
b. Create software to determine the vector, scalar and parametric equation of a plane.
"""
