#!/bin/env python3.4.2
import math

"""
This is a try to program quadratic function calculator
Program by Apparate Code

The purpose of this program is to compute the following equation
Given:

ax^2 + bx + c = 0
Find the root of the equation that is:
(x + m)(x + n) = 0
We should obtain:
x = -m and x = -n
"""

"""User interface"""
def ask_coefficient():
    coefficient_a = input("What is the coefficient of a? ")
    coefficient_b = input("What is the coefficient of b? ")
    coefficient_c = input("What is the coefficient of c? ")

    #Turn into integer
    coefficient_a = int(coefficient_a)
    coefficient_b = int(coefficient_b)
    coefficient_c = int(coefficient_c)

    """Calculator Engine"""
    """
    Completing the square method
    1. Divide all coefficient by a
    2. Turn c/a into -c/a which we will call nc
    2. e = (nb/2)^2
    4. f = e + nc
    3. sqrt(f)
    4. f1 = + , f2 = -
    5. -c + f1, -c + f2
    """

#Divide all coefficients by a
    new_coefficient_a = coefficient_a/coefficient_a
    new_coefficient_b = coefficient_b/coefficient_a
    new_coefficient_c = coefficient_c/coefficient_a

    print(new_coefficient_a)
    print(new_coefficient_b)
    print(new_coefficient_c)

    #Turn c to negative
    negative_new_coefficient_c = -(new_coefficient_c)
    #print(negative_new_coefficient_b)
    print(negative_new_coefficient_c)

    #Perfect square value obtained from ((b/a)/2)^2
    new_coefficient_b_2 = new_coefficient_b / 2
    perfect_square = new_coefficient_b_2**2
    print("new_coefficient_b_2" + str(new_coefficient_b_2))
    print(perfect_square)

    #Left side value
    left_side = perfect_square + negative_new_coefficient_c
    print(left_side)
    
    #Square root of left_side
    left_side = math.sqrt(left_side)
    print(left_side)

    #Square root give positive and negative values!
    left_side_positive = left_side
    left_side_negative = -(left_side)
    print(left_side_positive)
    print(left_side_negative)

    #Final answers by adding negative_new_coefficient_c and both left_side(s)
    positive_answer = -(new_coefficient_b_2 + left_side_positive)
    negative_answer = -(new_coefficient_b_2 + left_side_negative)
    
    #Print the answers
    print(positive_answer)
    print(negative_answer)
    
    def input_repeat_check():
        input_repeat = input("Enter r if you want to enter different values for the coefficients. n if you want to exit. ")
        
        if input_repeat == "n":
            print("Thank you for using this software!")
            print("Any feedbacks can be send to r3x.asadun@live.com")
            #End the program
        if input_repeat == "r":
                        ask_coefficient()
            #Restart the program
        else:
                        print("Wrong input: Please enter only y or n")
                        input_repeat_check()
                        #Repeat input checker
                        
    input_repeat_check()

ask_coefficient()

