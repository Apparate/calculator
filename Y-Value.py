"""This program is created to calculate the value of y given any value of x for any quadratic function"""

def start():
        def calculate(coefficient_a, coefficient_b, coefficient_c):
                x_value = input("What is the value of x? ")
                x_value = int(x_value)
                
                y_value = (coefficient_a * (x_value ** 2)) + (coefficient_b * x_value) + (coefficient_c)
                print(y_value) #The answer
                
                def input_repeat_check():
                        print("Do you want to find another y? y for yes, n for no ")
                        input_repeat = input("Enter r if you want to enter different values for the coefficients")
                        if input_repeat == "y":
                                calculate(coefficient_a, coefficient_b, coefficient_c)
                                #Restart calculate module
                                
                        if input_repeat == "n":
                                print("Thank you for using this software!")
                                print("Any feedbacks can be send to r3x.asadun@live.com")
                                #End the program
                                
                        if input_repeat == "r":
                                start()
                                #Restart the program
                                
                        else:
                                print("Wrong input: Please enter only y or n")
                                input_repeat_check()
                                #Repeat input checker
                input_repeat_check()

        #Coefficient a
        def c_check(coefficient_a, coefficient_b):
                coefficient_c = input("What is the value for coefficient of c? ")
                coefficient_c = int(coefficient_c)
                calculate(coefficient_a, coefficient_b, coefficient_c)

        #Coefficient b
        def b_check(coefficient_a):
                coefficient_b = input("What is the value for coefficient of b? ")
                coefficient_b = int(coefficient_b)
                c_check(coefficient_a, coefficient_b)

        #Coefficient c
        def a_check():
                coefficient_a = input("What is the value for coefficient of a? ")
                coefficient_a = int(coefficient_a)
                b_check(coefficient_a)

        #Initiate from a_check to b_check to c_check
        a_check()
                        
start()
